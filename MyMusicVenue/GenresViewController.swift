//
//  GenresViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 20.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class GenresViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            tableView?.tableFooterView = UIView()
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField? {
        didSet {
            searchTextField?.textColor = UIColor.white
            searchTextField?.attributedPlaceholder = NSAttributedString.init(string: "Search", attributes: [NSForegroundColorAttributeName: UIColor.white])
            searchTextField?.tintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1)
            searchTextField?.addTarget(self, action: #selector(searchTextFieldTextChanged), for: .editingChanged)
        }
    }
    
    let genresArray = ["Alternative", "Blues", "Country", "Electronic", "Jazz", "Pop", "R&B", "Rap/Hip-Hop", "Reggae", "Rock"]
    
    var selectedGenresSet: Set<String>?
    
    var searchArray = [String]()
    var useSearchArray = false
    var useMyEventsGenres = false
    var userDefaultsKey = ""

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tickTableViewCellNib = UINib(nibName: "TickTableViewCell", bundle: nil)
        tableView?.register(tickTableViewCellNib, forCellReuseIdentifier: "TickTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        userDefaultsKey = useMyEventsGenres ? "myEventsSelectedGenres" : "selectedGenres"
        
        if let genresData = UserDefaults.standard.object(forKey: userDefaultsKey) {
            selectedGenresSet = NSKeyedUnarchiver.unarchiveObject(with: genresData as! Data) as? Set<String>
        } else {
            selectedGenresSet = Set<String>()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Gesture recognizer
    
    func tapGesture() {
        searchTextField?.resignFirstResponder()
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressSaveButton() {
        let selectedGenresData = NSKeyedArchiver.archivedData(withRootObject: selectedGenresSet!)
        UserDefaults.standard.set(selectedGenresData, forKey: userDefaultsKey)
        UserDefaults.standard.synchronize()
        
        _ = navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource

extension GenresViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return useSearchArray ? searchArray.count : genresArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TickTableViewCell") as! TickTableViewCell
        cell.descriptionLabel?.text = useSearchArray ? searchArray[indexPath.row] : genresArray[indexPath.row]
        
        if (selectedGenresSet?.contains(cell.descriptionLabel!.text!))! {
            cell.tickImageView?.image = #imageLiteral(resourceName: "Tick")
        } else {
            cell.tickImageView?.image = nil
        }
        
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - UITableViewDelegate

extension GenresViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchTextField?.resignFirstResponder()
        
        let cell = tableView.cellForRow(at: indexPath) as! TickTableViewCell
        
        if (selectedGenresSet?.contains(cell.descriptionLabel!.text!))! {
            cell.tickImageView?.image = nil
            _ = selectedGenresSet?.remove(cell.descriptionLabel!.text!)
        } else {
            cell.tickImageView?.image = #imageLiteral(resourceName: "Tick")
            _ = selectedGenresSet?.update(with: cell.descriptionLabel!.text!)
        }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 17
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        headerView.backgroundColor = UIColor(red: 29.0 / 255.0, green: 30.0 / 255.0, blue: 34.0 / 255.0, alpha: 1)
        
        let redLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        redLineView.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        redLineView.center = headerView.center
        headerView.addSubview(redLineView)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 0))
        headerLabel.numberOfLines = 0
        headerLabel.backgroundColor = headerView.backgroundColor
        headerLabel.textColor = redLineView.backgroundColor
        headerLabel.font = UIFont.italicSystemFont(ofSize: 16);
        headerLabel.text = "Select genres"
        headerLabel.sizeToFit()
        headerLabel.frame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 15, height: headerLabel.frame.size.height)
        headerLabel.center = headerView.center
        headerLabel.textAlignment = .center
        
        headerView.addSubview(headerLabel)
        
        return headerView
    }
}

// MARK: - UITextFieldDelegate

extension GenresViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func searchTextFieldTextChanged() {
        if searchTextField!.text! == "" {
            useSearchArray = false
        } else {
            useSearchArray = true
            searchArray = genresArray.filter({ (genre) -> Bool in
                return genre.localizedCaseInsensitiveContains(self.searchTextField!.text!)
            })
        }
        
        tableView?.reloadData()
    }
}

// MARK: - UIScrollViewDelegate

extension GenresViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchTextField?.resignFirstResponder()
    }
}
