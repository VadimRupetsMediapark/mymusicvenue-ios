//
//  PreferencesMapsViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 20.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit
import GoogleMaps

class PreferencesMapsViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView?

    var locationManager = CLLocationManager()
    var marker = GMSMarker()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Set My Location"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1);
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(didPressSaveButton))
        navigationItem.rightBarButtonItem = saveButton
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 150
        locationManager.startUpdatingLocation()
        
        setupMap()
        
        if let userLocationDictionaryData = UserDefaults.standard.object(forKey: "userLocation") as? Data {
            if let userLocationDictionary = NSKeyedUnarchiver.unarchiveObject(with: userLocationDictionaryData) as? [String: Double] {
                placeMarker(coordinate: CLLocationCoordinate2DMake(userLocationDictionary["latitude"]!, userLocationDictionary["longitude"]!))
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if marker.map != nil {
            let cameraUpdate = GMSCameraUpdate.setTarget(marker.position, zoom: 15)
            mapView?.animate(with: cameraUpdate)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        locationManager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setup UI
    
    func setupMap() {
        mapView?.delegate = self
        mapView?.settings.myLocationButton = true
        if let location = locationManager.location?.coordinate {
            let cameraUpdate = GMSCameraUpdate.setTarget(location, zoom: 15)
            mapView?.animate(with: cameraUpdate)
        }
    }
    
    func placeMarker(coordinate: CLLocationCoordinate2D) {
        let cameraUpdate = GMSCameraUpdate.setTarget(coordinate, zoom: 15)
        mapView?.animate(with: cameraUpdate)
        mapView?.clear()
        
        marker = GMSMarker(position: coordinate)
        marker.title = "Your position"
        marker.map = mapView
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func didPressSaveButton() {
        if marker.map != nil {
            let userLocationDictionary = ["latitude": marker.position.latitude, "longitude": marker.position.longitude]
            let userLocationDictionaryData = NSKeyedArchiver.archivedData(withRootObject: userLocationDictionary)
            UserDefaults.standard.set(userLocationDictionaryData, forKey: "userLocation")
            UserDefaults.standard.synchronize()
            
            GMSGeocoder().reverseGeocodeCoordinate(marker.position, completionHandler: { (response, error) in
                if let results = response?.results() {
                    if let addressObject = results.first {
                        var addressString = ""
                        
                        if let country = addressObject.country {
                            addressString += country
                        }
                        
                        if let city = addressObject.locality {
                            if addressString != "" {
                                addressString += ", " + city
                            } else {
                                addressString += city
                            }
                        }
                        
                        UserDefaults.standard.set(addressString, forKey: "currentAddress")
                        UserDefaults.standard.synchronize()
                    }
                }
                
                _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension PreferencesMapsViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate {
            let cameraUpdate = GMSCameraUpdate.setTarget(location, zoom: 15)
            mapView?.animate(with: cameraUpdate)
            
            if marker.map == nil {
                placeMarker(coordinate: location)
            }
        }
    }
}

// MARK: - GMSMapViewDelegate

extension PreferencesMapsViewController: GMSMapViewDelegate {
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if let coordinate = locationManager.location?.coordinate {
            let cameraUpdate = GMSCameraUpdate.setTarget(coordinate, zoom: 15)
            mapView.animate(with: cameraUpdate)
            placeMarker(coordinate: coordinate)
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let cameraUpdate = GMSCameraUpdate.setTarget(coordinate, zoom: 15)
        mapView.animate(with: cameraUpdate)
        placeMarker(coordinate: coordinate)
    }
}
