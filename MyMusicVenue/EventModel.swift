//
//  EventModel.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 26.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import Foundation
import CoreLocation

class EventId: NSObject, NSCoding {
    let id: String
    
    override init() {
        self.id = ""
        
        super.init()
    }
    
    init(eventId: String) {
        self.id = eventId
        
        super.init()
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let eventId = aDecoder.decodeObject(forKey: "id") as! String
        
        self.init(eventId: eventId)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
    }
}

class Location: NSObject, NSCoding {
    let address1: String
    let address2: String
    let city: String
    let locationDescription: String
    let location: CLLocationCoordinate2D
    let name: String
    let state: String
    let zip: String
    
    override init() {
        self.address1 = ""
        self.address2 = ""
        self.city = ""
        self.locationDescription = ""
        self.location = CLLocationCoordinate2DMake(0, 0)
        self.name = ""
        self.state = ""
        self.zip = ""
        
        super.init()
    }
    
    init(address1: String, address2: String, city: String, description: String, latitude: Double, longitude: Double, name: String, state: String, zip: String) {
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.locationDescription = description
        self.location = CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude)
        self.name = name
        self.state = state
        self.zip = zip
        
        super.init()
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let address1 = aDecoder.decodeObject(forKey: "address1") as! String
        let address2 = aDecoder.decodeObject(forKey: "address2") as! String
        let city = aDecoder.decodeObject(forKey: "city") as! String
        let locationDescription = aDecoder.decodeObject(forKey: "locationDescription") as! String
        let latitude = aDecoder.decodeDouble(forKey: "latitude")
        let longitude = aDecoder.decodeDouble(forKey: "longitude")
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let state = aDecoder.decodeObject(forKey: "state") as! String
        let zip = aDecoder.decodeObject(forKey: "zip") as! String
        
        self.init(address1: address1, address2: address2, city: city, description: locationDescription, latitude: latitude, longitude: longitude, name: name, state: state, zip: zip)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(address1, forKey: "address1")
        aCoder.encode(address2, forKey: "address2")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(locationDescription, forKey: "locationDescription")
        aCoder.encode(location.latitude, forKey: "latitude")
        aCoder.encode(location.longitude, forKey: "longitude")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(zip, forKey: "zip")
    }
}

class MediaFile: NSObject, NSCoding {
    let mediaFileURL: String
    let thumbnailURL: String
    let type: String
    
    override init() {
        self.mediaFileURL = ""
        self.thumbnailURL = ""
        self.type = ""
        
        super.init()
    }
    
    init(mediaFileURL: String, thumbnailURL: String, type: String) {
        self.mediaFileURL = mediaFileURL
        self.thumbnailURL = thumbnailURL
        self.type = type
        
        super.init()
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let mediaFileURL = aDecoder.decodeObject(forKey: "mediaFileURL") as! String
        let thumbnailURL = aDecoder.decodeObject(forKey: "thumbnailURL") as! String
        let type = aDecoder.decodeObject(forKey: "type") as! String
        
        self.init(mediaFileURL: mediaFileURL, thumbnailURL: thumbnailURL, type: type)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(mediaFileURL, forKey: "mediaFileURL")
        aCoder.encode(thumbnailURL, forKey: "thumbnailURL")
        aCoder.encode(type, forKey: "type")
    }
}

class Event: NSObject {
    let id: EventId
    let ends: Date?
    let genres: [String]
    let location: Location
    let mediaFiles: [MediaFile]
    let name: String
    let starts: Date?
    let type: String
    
    override init() {
        self.id = EventId()
        self.ends = nil
        self.genres = [""]
        self.location = Location()
        self.mediaFiles = [MediaFile]()
        self.name = ""
        self.starts = nil
        self.type = ""
        
        super.init()
    }
    
    init(dictionary: [String: Any]) {
        
        // ID parsing
        
        if let id = dictionary["_id"] as? [String: String] {
            if let eventId = id["$id"] {
                self.id = EventId.init(eventId: eventId)
            } else {
                self.id = EventId.init(eventId: "")
            }
        } else {
            self.id = EventId.init(eventId: "")
        }
        
        // Ends date parsing
        
        if let endsString = dictionary["ends"] as? NSString {
            var timezoneString = endsString.substring(with: NSMakeRange(endsString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = endsString.substring(with: NSMakeRange(0, endsString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + " " + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.ends = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.ends = nil
        }
        
        // Genres parsing
        
        if let genres = dictionary["genres"] as? [String] {
            self.genres = genres
        } else {
            self.genres = [String]()
        }
        
        // Location parsing
        
        if let locationDictionary = dictionary["location"] as? [String: AnyObject] {
            var locationAddress1 = ""
            var locationAddress2 = ""
            var locationCity = ""
            var locationDescription = ""
            var locationLatitude: Double = 0
            var locationLongitude: Double = 0
            var locationName = ""
            var locationState = ""
            var locationZip = ""
            
            if let address1 = locationDictionary["address_1"] as? String {
                locationAddress1 = address1
            }
            
            if let address2 = locationDictionary["address_2"] as? String {
                locationAddress2 = address2
            }
            
            if let city = locationDictionary["city"] as? String {
                locationCity = city
            }
            
            if let description = locationDictionary["description"] as? String {
                locationDescription = description
            }
            
            if let locationCoordinate = locationDictionary["location"] as? [String: Double] {
                if let latitude = locationCoordinate["lat"] {
                    locationLatitude = latitude//.truncate(places: 4)
                }
                
                if let longitude = locationCoordinate["lng"] {
                    locationLongitude = longitude//.truncate(places: 4)
                }
            }
            
            if let name = locationDictionary["name"] as? String {
                locationName = name
            }
            
            if let state = locationDictionary["state"] as? String {
                locationState = state
            }
            
            if let zip = locationDictionary["zip"] as? String {
                locationZip = zip
            }
            
            self.location = Location(address1: locationAddress1, address2: locationAddress2, city: locationCity, description: locationDescription, latitude: locationLatitude, longitude: locationLongitude, name: locationName, state: locationState, zip: locationZip)
        } else {
            self.location = Location(address1: "", address2: "", city: "", description: "", latitude: 0, longitude: 0, name: "", state: "", zip: "")
        }
        
        // MediaFiles parsing
        
        if let mediaFilesArray = dictionary["media_files"] as? [[String: String]] {
            var mediaFiles = [MediaFile]()
            
            for mediaFilesDictionary in mediaFilesArray {
                var mediaFileURL = ""
                var thumbnailURL = ""
                var type = ""
                
                if let fileURL = mediaFilesDictionary["media_file_url"] {
                    mediaFileURL = fileURL
                }
                
                if let thumbURL = mediaFilesDictionary["thumbnail_url"] {
                    thumbnailURL = thumbURL
                }
                
                if let typeImage = mediaFilesDictionary["type"] {
                    type = typeImage
                }
                
                mediaFiles.append(MediaFile.init(mediaFileURL: mediaFileURL, thumbnailURL: thumbnailURL, type: type))
            }
            
            self.mediaFiles = mediaFiles
        } else {
            self.mediaFiles = [MediaFile]()
        }
        
        // Event name parsing
        
        if let eventName = dictionary["name"] as? String {
            self.name = eventName
        } else {
            self.name = ""
        }
        
        // Starts date parsing
        
        if let startsString = dictionary["starts"] as? NSString {
            var timezoneString = startsString.substring(with: NSMakeRange(startsString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = startsString.substring(with: NSMakeRange(0, startsString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + " " + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.starts = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.starts = nil
        }
        
        // Event type parsing
        
        if let eventType = dictionary["type"] as? String {
            self.type = eventType
        } else {
            self.type = ""
        }
        
        super.init()
    }
}

extension Date {
    func shortDescription() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy"
        dateFormatter.locale = Locale.init(identifier: "en-US")
        return dateFormatter.string(from: self).capitalized
    }
    
    func timeDescription() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self)
    }
}

public extension Double {
    func truncate(places: Int) -> Double {
        return Double(floor(pow(10.0, Double(places)) * self) / pow(10.0, Double(places)))
    }
}
