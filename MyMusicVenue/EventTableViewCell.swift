//
//  EventTableViewCell.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 20.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var artistLogoImageView: UIImageView?
    @IBOutlet weak var artistLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var placeLabel: UILabel?
    @IBOutlet weak var genreLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
