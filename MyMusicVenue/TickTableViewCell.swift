//
//  TickTableViewCell.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 20.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class TickTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var tickImageView: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
