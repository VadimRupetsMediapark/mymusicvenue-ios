//
//  TutorialFirstViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 19.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class TutorialFirstViewController: UIViewController {
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
