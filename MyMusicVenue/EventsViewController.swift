//
//  EventsViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 21.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import GoogleMapsCore
import Alamofire
import JGProgressHUD

enum EventType {
    case All
    case Concert
    case Festival
    case Party
    case LivePerformance
}

class UIButtonScrollView: UIScrollView {
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }
}

class EventsViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var mainViewLeftConstraint: NSLayoutConstraint?
    @IBOutlet weak var mainViewRightConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var eventButtonsScrollView: UIButtonScrollView?
    @IBOutlet var eventsButtonsCollection: [UIButton]?
    @IBOutlet weak var eventLineViewWidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var eventLineViewLeftConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var eventsScrollView: UIScrollView?
    
    @IBOutlet var eventTableViewCollection: [UITableView]?
    
    @IBOutlet weak var searchTextField: UITextField? {
        didSet {
            searchTextField?.textColor = UIColor.white
            searchTextField?.attributedPlaceholder = NSAttributedString.init(string: "Search", attributes: [NSForegroundColorAttributeName: UIColor.white])
            searchTextField?.tintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var mapView: GMSMapView? {
        didSet {
            mapView?.delegate = self
        }
    }
    var clusterManager: GMUClusterManager!
    
    @IBOutlet weak var mapViewEventsPopup: UIView? {
        didSet {
            mapViewEventsPopup?.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            mapViewEventsPopup?.layer.cornerRadius = 10
            mapViewEventsPopup?.layer.borderWidth = 2
            mapViewEventsPopup?.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var mapViewEventsPopupBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var mapViewEventsPopupTopConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var mapViewEventsPopupTableView: UITableView? {
        didSet {
            let tableViewNib = UINib(nibName: "EventTableViewCell", bundle: nil)
            mapViewEventsPopupTableView?.register(tableViewNib, forCellReuseIdentifier: "EventTableViewCell")
        }
    }
    
    @IBOutlet weak var filterButton: UIButton?
    var isFilterButtonFilter = true
    
    @IBOutlet weak var mapOrListButton: UIButton? {
        didSet {
            mapOrListButton?.imageView?.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var mapOrListButtonHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var mapOrListButtonBottomConstraint: NSLayoutConstraint?
    var isMapButtonMap = false
    
    var currentPage = 0
    
    var shouldScroll = true
    var lastContentOffset: CGFloat = 0
    
    var eventsArray = [[Event]]()
    var searchResultsArray = [[Event]]()
    var useSearchResults = false
    
    let eventsTypes = ["Concert", "Festival", "Party", "Live Performance"]
    
    var selectedGenresSet: Set<String> = ["Hello World"]
    var selectedDate = ""
    
    var clusterEventsArray = [Event]()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshEvents()
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView!, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView!, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)

        let tableViewNib = UINib(nibName: "EventTableViewCell", bundle: nil)
        for tableView in eventTableViewCollection! {
            tableView.register(tableViewNib, forCellReuseIdentifier: "EventTableViewCell")
        }
        
        searchTextField?.addTarget(self, action: #selector(toggleFilterButton), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var isRefreshNeeded = false
        
        if let genresData = UserDefaults.standard.object(forKey: "selectedGenres") as? Data {
            if let unarchivedSet = NSKeyedUnarchiver.unarchiveObject(with: genresData) as? Set<String> {
                if selectedGenresSet != unarchivedSet {
                    selectedGenresSet = unarchivedSet
                    isRefreshNeeded = true
                }
            }
        }
        
        if let savedSelectedDate = UserDefaults.standard.string(forKey: "selectedDate") {
            if selectedDate != savedSelectedDate {
                selectedDate = savedSelectedDate
                isRefreshNeeded = true
            }
        }
        
        if isRefreshNeeded {
            refreshEvents()
        }
        
        mapViewEventsPopup?.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        eventsScrollView?.contentSize = CGSize(width: view.frame.size.width * 5, height: eventsScrollView!.frame.size.height)
    }
    
    // MARK: - Show alertController method
    
    func showAlertController(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Refresh events methods
    
    func refreshEvents() {
        let progressHUD = JGProgressHUD.init(style: .dark)
        progressHUD?.textLabel.text = "Loading"
        progressHUD?.show(in: view)
        
        var parameters: [String: Any]?
        
        if selectedGenresSet.count != 0 && selectedGenresSet.count != 10 {
            parameters = ["genres": [String](selectedGenresSet)]
        }
        
        if selectedDate != "All" {
            var selectedDateParameterValue = ""
            switch selectedDate {
            case "Today":
                selectedDateParameterValue = "today"
            case "Now":
                selectedDateParameterValue = "now"
            case "This week":
                selectedDateParameterValue = "week"
            default:
                selectedDateParameterValue = "month"
            }
            
            if parameters != nil {
                parameters?["event_date"] = selectedDateParameterValue
            } else {
                parameters = ["event_date": selectedDateParameterValue]
            }
        }
        
        Alamofire.request(URL(string: "http://mymusicvenue.com/app/events")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if (response.result.isFailure) {
                self.showAlertController(title: "Error", message: response.result.error!.localizedDescription)
            } else {
                let result = response.result.value as! [String: Any]
                let responseEventsArray = result["records"] as! [[String: AnyObject]]
                
                self.eventsArray = [[Event]]()
                var allEvents = [Event]()
                
                DispatchQueue.global(qos: .background).async {
                    for eventDictionary in responseEventsArray {
                        let event = Event.init(dictionary: eventDictionary)
                        allEvents.append(event)
                    }
                    
                    self.eventsArray.append(allEvents)
                    for index in 0..<4 {
                        self.eventsArray.append(self.eventsArray[0].filter({ (event) -> Bool in
                            return event.type == self.eventsTypes[index]
                        }))
                    }
                    
                    if !(self.mapView?.isHidden)! {
                        self.refreshCluster()
                    }
                    
                    DispatchQueue.main.async {
                        for tableView in self.eventTableViewCollection! {
                            tableView.reloadData()
                        }
                        
                        progressHUD?.dismiss(animated: true)
                    }
                }
            }
        }
    }
    
    func refreshCluster() {
        clusterManager.clearItems()
        
        if let firstEvent = (useSearchResults ? searchResultsArray : eventsArray)[currentPage].first {
            let cameraUpdate = GMSCameraUpdate.setTarget(firstEvent.location.location, zoom: 10)
            mapView?.animate(with: cameraUpdate)
        }
        
        for event in (useSearchResults ? searchResultsArray : eventsArray)[currentPage] {
            let item = POIItem(position: event.location.location, event: event)
            clusterManager.add(item)
        }
        
        clusterManager.cluster()
    }
    
    // MARK: - Menu show/hide func
    
    func showMenu() {
        mainViewLeftConstraint?.constant = 150
        mainViewRightConstraint?.constant -= 150
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideMenu() {
        mainViewLeftConstraint?.constant = 0
        mainViewRightConstraint?.constant = 0
        
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - TextField Event
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        toggleFilterButton()
    }
    
    func toggleFilterButton() {
        if searchTextField?.text == "" {
            filterButton?.setImage(#imageLiteral(resourceName: "Filter"), for: .normal)
            isFilterButtonFilter = true
            useSearchResults = false
        } else {
            filterButton?.setImage(#imageLiteral(resourceName: "X icon"), for: .normal)
            isFilterButtonFilter = false
            useSearchResults = true
            
            filterEventsByName()
        }
        
        for tableView in self.eventTableViewCollection! {
            tableView.reloadData()
        }
        
        refreshCluster()
    }
    
    func filterEventsByName() {
        searchResultsArray = [[Event]]()
        searchResultsArray.append(eventsArray[0].filter({ (event) -> Bool in
            return event.name.localizedCaseInsensitiveContains(searchTextField!.text!)
        }))
        
        for index in 0..<4 {
            searchResultsArray.append(searchResultsArray[0].filter({ (event) -> Bool in
                return event.name.contains(eventsTypes[index])
            }))
        }
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressMenuButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if mainViewLeftConstraint?.constant == 0 {
            showMenu()
        } else {
            hideMenu()
        }
        
        searchTextField?.resignFirstResponder()
    }
    
    @IBAction func didPressEventButton(sender: UIButton) {
        currentPage = sender.tag
        eventButtonAction(senderTag: sender.tag, dontRotate: false)
    }
    
    func eventButtonAction(senderTag: Int, dontRotate: Bool) {
        for button in eventsButtonsCollection! {
            button.setTitleColor(UIColor(red: 90.0 / 255.0, green: 88.0 / 255.0, blue: 100.0 / 255.0, alpha: 1), for: .normal)
        }
        
        eventsButtonsCollection?[senderTag].setTitleColor(UIColor.white, for: .normal)
        
        eventLineViewWidthConstraint?.constant = eventsButtonsCollection![senderTag].frame.size.width
        eventLineViewLeftConstraint?.constant = eventsButtonsCollection![senderTag].frame.origin.x
        
        if senderTag >= 3 {
            let visibleRect = CGRect(x: eventButtonsScrollView!.contentSize.width - view.frame.size.width, y: 0, width: view.frame.size.width, height: eventButtonsScrollView!.frame.size.height)
            eventButtonsScrollView?.scrollRectToVisible(visibleRect, animated: true)
        } else {
            let visibleRect = CGRect(x: 0, y: 0, width: view.frame.size.width, height: eventButtonsScrollView!.frame.size.height)
            eventButtonsScrollView?.scrollRectToVisible(visibleRect, animated: true)
        }
        
        if !dontRotate {
            shouldScroll = false
            eventsScrollView?.scrollRectToVisible(CGRect(x: (CGFloat(senderTag)) * view.frame.size.width, y: 0, width: view.frame.size.width, height: eventsScrollView!.frame.size.height), animated: false)
            shouldScroll = true
        }
        
        view.layoutIfNeeded()
    }
    
    @IBAction func didPressContactUsButton() {
        performSegue(withIdentifier: "ContactUsViewController", sender: nil)
    }
    
    @IBAction func didPressSettingsButton() {
        performSegue(withIdentifier: "SetPreferencesViewController", sender: nil)
    }
    
    @IBAction func didPressMySquadButton() {
        performSegue(withIdentifier: "MySquadViewController", sender: nil)
    }
    
    @IBAction func didPressFilterButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if isFilterButtonFilter {
            performSegue(withIdentifier: "FilterPageViewController", sender: nil)
        } else {
            searchTextField?.text = ""
            toggleFilterButton()
        }
    }
    
    @IBAction func didPressMapOrListButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if isMapButtonMap {
            mapOrListButtonHeightConstraint?.constant = 39
            mapOrListButtonBottomConstraint?.constant -= 6
            mapOrListButton?.setImage(#imageLiteral(resourceName: "Map Icon"), for: .normal)
            self.view.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.35, animations: { 
                self.mapView?.alpha = 0.0
                }, completion: { (completion) in
                    self.mapView?.isHidden = true
            })
        } else {
            mapOrListButtonHeightConstraint?.constant = 26
            mapOrListButtonBottomConstraint?.constant += 6
            mapOrListButton?.setImage(#imageLiteral(resourceName: "List"), for: .normal)
            self.view.layoutIfNeeded()
            
            refreshCluster()
            
            mapView?.isHidden = false
            UIView.animate(withDuration: 0.35, animations: { 
                self.mapView?.alpha = 1.0
            })
        }
        
        isMapButtonMap = !isMapButtonMap
    }
    
    @IBAction func didPressMapViewEventsClosePopupButton() {
        UIView.animate(withDuration: 0.5, animations: {
            self.mapViewEventsPopup?.alpha = 0.0
        })
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        mainViewLeftConstraint?.constant = 0
        mainViewRightConstraint?.constant = 0
        searchTextField?.resignFirstResponder()
        
        UIView.animate(withDuration: 1.5) {
            self.view.layoutIfNeeded()
        }
        
        if segue.identifier == "DetailEventViewController" {
            if let detailEvent = sender as? DetailEvent {
                let detailEventViewController = segue.destination as! DetailEventViewController
                detailEventViewController.detailEvent = detailEvent
            }
        }
    }

}

// MARK: - UIScrollViewDelegate

extension EventsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == 0 {
            return
        }
        
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = lroundf(Float(fractionalPage))
        
        if currentPage != page {
            currentPage = page
            if shouldScroll {
                eventButtonAction(senderTag: page, dontRotate: true)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchTextField?.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource

extension EventsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 5 {
            return clusterEventsArray.count
        } else {
            if (useSearchResults ? searchResultsArray : eventsArray).indices.contains(tableView.tag) {
                return (useSearchResults ? searchResultsArray : eventsArray)[tableView.tag].count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as! EventTableViewCell
        cell.selectionStyle = .none
        
        let event = tableView.tag == 5 ? clusterEventsArray[indexPath.row] : (useSearchResults ? searchResultsArray : eventsArray)[tableView.tag][indexPath.row]
        var genresString = ""
        
        for genre in event.genres {
            genresString += genre + " "
        }
        
        var dateLabelText = ""
        if let eventDate = event.starts {
            dateLabelText += eventDate.shortDescription()
        }
        
        
        cell.artistLabel?.text = event.name
        cell.placeLabel?.text = event.location.name
        cell.genreLabel?.text = genresString
        cell.dateLabel?.text = dateLabelText
        
//        if let thumbnailURL = event.mediaFiles.first?.thumbnailURL {
//            cell.artistLogoImageView?.sd_setImage(with: URL(string: thumbnailURL))
//        }
        
        cell.artistLogoImageView?.image = #imageLiteral(resourceName: "Event thumbnail")
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension EventsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = tableView.tag == 5 ? self.clusterEventsArray[indexPath.row] : self.eventsArray[tableView.tag][indexPath.row]
        let eventId = event.id.id
        
        let progressHUD = JGProgressHUD.init(style: .dark)
        progressHUD?.textLabel.text = "Loading"
        progressHUD?.show(in: view)
        Alamofire.request(URL(string: "http://mymusicvenue.com/app/events/event/\(eventId)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if (response.result.isFailure) {
                self.showAlertController(title: "Error", message: response.result.error!.localizedDescription)
            } else {
                let result = response.result.value as! [String: Any]
                
                let detailEvent = DetailEvent(dictionary: result)
                progressHUD?.dismiss(animated: true)
                self.performSegue(withIdentifier: "DetailEventViewController", sender: detailEvent as Any)
            }
        }
    }
}

// MARK: - UITextFieldDelegate

extension EventsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if mapViewEventsPopup?.alpha == 1.0 {
            return false
        } else {
            return true
        }
    }
}

// MARK: - MapViewMethods

extension EventsViewController {
    func didTap(clusterItems: [POIItem]) {
        let progressHUD = JGProgressHUD.init(style: .dark)
        progressHUD?.textLabel.text = "Loading"
        progressHUD?.show(in: view)
        
        DispatchQueue.global(qos: .background).async {
            self.clusterEventsArray = [Event]()
            
            for poiItem in clusterItems {
                self.clusterEventsArray.append(poiItem.event)
            }
            
            var missedEvents = [Event]()
            
            for event in self.clusterEventsArray {
                
                let samePositionEvents = (self.useSearchResults ? self.searchResultsArray : self.eventsArray)[self.currentPage].filter({ (specificEvent) -> Bool in
                    return specificEvent.location.location.latitude == event.location.location.latitude && specificEvent.location.location.longitude == event.location.location.longitude
                })
                
                missedEvents.append(contentsOf: samePositionEvents)
            }
            
            self.clusterEventsArray.append(contentsOf: missedEvents)
            self.clusterEventsArray = Array<Event>(Set<Event>(self.clusterEventsArray))
            
            let cellsHeight = self.clusterEventsArray.count * 100 + 16
            let constraintConstant = (self.view.frame.size.height - 25 - 8 - 16 - 40 - CGFloat(cellsHeight)) / 2.0
            
            DispatchQueue.main.async {
                self.mapViewEventsPopupTableView?.reloadData()
                
                if constraintConstant > 0 {
                    self.mapViewEventsPopupTableView?.sizeToFit()
                    self.mapViewEventsPopupTableView?.isScrollEnabled = false
                    
                    let constant = (self.view.frame.size.height - CGFloat(cellsHeight)) / 2.0
                    
                    self.mapViewEventsPopupTopConstraint?.constant = constant - 26
                    self.mapViewEventsPopupBottomConstraint?.constant = constant - 26
                } else {
                    self.mapViewEventsPopupTopConstraint?.constant = 25
                    self.mapViewEventsPopupBottomConstraint?.constant = 8
                    
                    self.mapViewEventsPopupTableView?.isScrollEnabled = true
                }
                
                self.mapViewEventsPopup?.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.35, animations: {
                    self.mapViewEventsPopup?.alpha = 1.0
                }, completion: { (completion) in
                    progressHUD?.dismiss(animated: true)
                })
            }
        }
    }
}

extension EventsViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            didTap(clusterItems: [poiItem])
        } else {
            print("marker")
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if gesture {
            searchTextField?.resignFirstResponder()
        }
    }
}

extension EventsViewController: GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) {
        if let clusterItems = cluster.items as? [POIItem] {
            didTap(clusterItems: clusterItems)
        }
    }
}
