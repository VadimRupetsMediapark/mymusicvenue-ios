//
//  DetailTableViewCell.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 25.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var titleLabelWidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var descriptionLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
