//
//  DetailEventViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 25.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit
import EventKit
import JGProgressHUD
import Alamofire
import SDWebImage

class DetailEventViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            let detailTableViewCellNib = UINib(nibName: "DetailTableViewCell", bundle: nil)
            let playingTableViewCellNib = UINib(nibName: "PlayingTableViewCell", bundle: nil)
            let commentsTableViewCellNib = UINib(nibName: "CommentsTableViewCell", bundle: nil)
            
            tableView?.register(detailTableViewCellNib, forCellReuseIdentifier: "DetailTableViewCell")
            tableView?.register(playingTableViewCellNib, forCellReuseIdentifier: "PlayingTableViewCell")
            tableView?.register(commentsTableViewCellNib, forCellReuseIdentifier: "CommentsTableViewCell")
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel?
    
    var headerScrollView: UIScrollView?
    var headerPageControl: UIPageControl?
    
    var currentPage = 0
    
    var eventId = ""
    var detailEvent = DetailEvent()
    var eventLoaded: Bool = false
    var eventAddedToCalendar: Bool = false
    
    var myEvents = [DetailEvent]()

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshMyEvents()
        
        eventLoaded = true
        eventId = detailEvent.id.id
        
        titleLabel?.text = detailEvent.name
        
//        setupTableViewHeader()
//        setupTableViewFooter()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupTableViewFooter()
        setupTableViewHeader()
    }
    
    // MARK: - Setup UI
    
    func setupTableViewHeader() {
        let screenWidth = UIScreen.main.bounds.width
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth - 16, height: 275))
        headerView.backgroundColor = UIColor.clear
        
        headerScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: headerView.frame.size.width, height: 200))
        headerScrollView!.isPagingEnabled = true
        headerScrollView!.showsHorizontalScrollIndicator = false
        headerScrollView!.tag = 99
        headerScrollView!.delegate = self
        headerScrollView?.contentSize.width = CGFloat(detailEvent.mediaFiles.count) * screenWidth - 16
        for (index, mediaFile) in detailEvent.mediaFiles.enumerated() {
            let imageView = UIImageView(frame: CGRect(x: CGFloat(index) * screenWidth, y: 0, width: screenWidth - 16, height: 200))
            imageView.contentMode = .scaleAspectFit
//            imageView.sd_setImage(with: URL(string: mediaFile.mediaFileURL))
            imageView.image = #imageLiteral(resourceName: "Event preview")
            imageView.backgroundColor = UIColor.clear
            
            headerScrollView!.addSubview(imageView)
        }
        headerView.addSubview(headerScrollView!)
        
        headerPageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        headerPageControl!.numberOfPages = detailEvent.mediaFiles.count
        headerPageControl!.tag = 98
        headerPageControl!.sizeToFit()
        headerPageControl!.frame = CGRect(x: (screenWidth - headerPageControl!.frame.size.width - 4) / 2.0, y: 189.5 - (headerPageControl!.frame.size.height / 2.0), width: headerPageControl!.frame.size.width, height: headerPageControl!.frame.size.height)
        
        headerView.addSubview(headerPageControl!)
        
        let addToMyEventsButton = UIButton()
        addToMyEventsButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        addToMyEventsButton.contentHorizontalAlignment = .left
        addToMyEventsButton.setTitle("Add to my events", for: .normal)
        addToMyEventsButton.setTitleColor(UIColor.white, for: .normal)
        addToMyEventsButton.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 0.5), for: .highlighted)
        addToMyEventsButton.frame = CGRect(x: 8, y: 208, width: headerView.frame.size.width - 8, height: headerView.frame.size.height - 208)
        addToMyEventsButton.addTarget(self, action: #selector(didPressAddToMyEventsButton), for: .touchUpInside)
        headerView.addSubview(addToMyEventsButton)
        
        let tickImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        tickImageView.contentMode = .scaleAspectFit
        tickImageView.image = myEventsContains(specificEvent: detailEvent) ? #imageLiteral(resourceName: "Tick") : nil
        tickImageView.frame = CGRect(x: headerView.frame.size.width - 8 - 16, y: 0, width: 16, height: 16)
        tickImageView.center.y = addToMyEventsButton.center.y
        tickImageView.tag = 97
        headerView.addSubview(tickImageView)
        
        tableView?.tableHeaderView = headerView
    }
    
    func setupTableViewFooter() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width - 16, height: 90))
        footerView.backgroundColor = UIColor.clear
        
        let addEventToCalendarButtton = UIButton()
        addEventToCalendarButtton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        addEventToCalendarButtton.setTitleColor(UIColor.white, for: .normal)
        addEventToCalendarButtton.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 0.5), for: .highlighted)
        addEventToCalendarButtton.setTitle("Add event to calendar", for: .normal)
        addEventToCalendarButtton.sizeToFit()
        addEventToCalendarButtton.frame.size.width = footerView.frame.size.width
        addEventToCalendarButtton.frame.origin = CGPoint(x: 0, y: 75 - addEventToCalendarButtton.frame.size.height)
        addEventToCalendarButtton.addTarget(self, action: #selector(didPressAddEventToCalendarButton), for: .touchUpInside)
        footerView.addSubview(addEventToCalendarButtton)
        
        tableView?.tableFooterView = footerView
    }
    
    // MARK: - MyEvents methods
    
    func refreshMyEvents() {
        if let savedMyEventsData = UserDefaults.standard.object(forKey: "myEvents") as? Data {
            if let savedMyEvents = NSKeyedUnarchiver.unarchiveObject(with: savedMyEventsData) as? [DetailEvent] {
                myEvents = savedMyEvents
            } else {
                myEvents = [DetailEvent]()
            }
        } else {
            myEvents = [DetailEvent]()
        }
    }
    
    func myEventsContains(specificEvent: DetailEvent) -> Bool {
        for event in myEvents {
            if event.id.id == specificEvent.id.id {
                return true
            }
        }
        
        return false
    }
    
    func myEventsRemove(specificEvent: DetailEvent) {
        var indexForRemoving = -1
        
        for (index, event) in myEvents.enumerated() {
            if event.id.id == specificEvent.id.id {
                indexForRemoving = index
                break
            }
        }
        
        myEvents.remove(at: indexForRemoving)
    }
    
    // MARK: - Buttons Actions

    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func didPressAddToMyEventsButton() {
        let tickImageView = view.viewWithTag(97) as! UIImageView
        
        if myEventsContains(specificEvent: detailEvent) {
            myEventsRemove(specificEvent: detailEvent)
            tickImageView.image = nil
        } else {
            myEvents.append(detailEvent)
            tickImageView.image = #imageLiteral(resourceName: "Tick")
        }
        
        let myEventsData = NSKeyedArchiver.archivedData(withRootObject: myEvents)
        UserDefaults.standard.set(myEventsData, forKey: "myEvents")
        UserDefaults.standard.synchronize()
    }
    
    func didPressAddEventToCalendarButton() {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event) { (granted, error) in
            if granted && error == nil {
                
                let event = EKEvent(eventStore: eventStore)
                var doesEventExist = false
                
                if let startDate = self.detailEvent.starts {
                    let predicate = eventStore.predicateForEvents(withStart: startDate, end: Date(timeInterval: 60 * 60 * 3, since: startDate), calendars: eventStore.calendars(for: .event))
                    let events = eventStore.events(matching: predicate)
                    
                    for event in events {
                        if event.title == self.detailEvent.name && event.notes == self.detailEvent.eventDescription {
                            doesEventExist = true
                            break
                        }
                    }
                }
                
                if !doesEventExist {
                    event.title = self.detailEvent.name
                    
                    if let endDate = self.detailEvent.ends {
                        event.endDate = endDate
                    }
                    
                    if let startDate = self.detailEvent.starts {
                        event.startDate = startDate
                        
                        if let endDate = self.detailEvent.ends {
                            event.endDate = endDate
                        } else {
                            event.endDate = startDate.addingTimeInterval(60 * 60 * 3)
                        }
                    }
                    
                    event.notes = self.detailEvent.eventDescription
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    
                    var eventId = ""
                    
                    do {
                        try eventStore.save(event, span: .thisEvent)
                        eventId = event.eventIdentifier
                    }
                    catch let error as NSError {
                        print("json error: \(error.localizedDescription)")
                        let alertController = UIAlertController(title: "Failure", message: error.localizedDescription, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alertController.addAction(okAction)
                        
                        DispatchQueue.main.async {
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                    if eventId != "" {
                        let alertController = UIAlertController(title: "Success", message: "This event has been added to your calendar", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alertController.addAction(okAction)
                        
                        DispatchQueue.main.async {
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                } else {
                    let alertController = UIAlertController(title: "This event is in your calendar", message: "You have already added event to your calendar", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertController.addAction(okAction)
                    
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            } else {
                let alertController = UIAlertController(title: "Permisson is not granted", message: "You should allow application to use your calendar", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func didPressGenreButton(sender: UIButton) {
        let selectedGenre = detailEvent.genres[sender.tag - 30]
        
        var genresSet = Set<String>()
        genresSet.update(with: selectedGenre)
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: genresSet), forKey: "selectedGenres")
        UserDefaults.standard.synchronize()
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EventMapViewSegue" {
            let eventMapViewController = segue.destination as! EventMapViewController
            
            eventMapViewController.eventTitle = detailEvent.name
            eventMapViewController.eventLocation = detailEvent.location.location
        }
    }
}

// MARK: - UITableViewDataSource

extension DetailEventViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 && indexPath.row == 1 {
            let playingCell = tableView.dequeueReusableCell(withIdentifier: "PlayingTableViewCell") as! PlayingTableViewCell
            playingCell.selectionStyle = .none
            
            var startX: CGFloat = 8
            var startY: CGFloat = 37
            var buttonTag = 30
            
            let cellWidth = view.frame.size.width - 16
            
            for genre in detailEvent.genres {
                let button = UIButton()
                button.setTitle(genre, for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                button.setTitleColor(UIColor.init(red: 213.0 / 255.0, green: 12.0 / 255.0, blue: 23.0 / 255.0, alpha: 1), for: .normal)
                button.setTitleColor(UIColor.init(red: 213.0 / 255.0, green: 12.0 / 255.0, blue: 23.0 / 255.0, alpha: 0.5), for: .highlighted)
                button.tag = buttonTag
                buttonTag += 1
                button.addTarget(self, action: #selector(didPressGenreButton(sender:)), for: .touchUpInside)
                button.sizeToFit()
                
                if cellWidth - startX - 8 >= button.frame.size.width {
                    button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
                    startX += button.frame.size.width + 8
                } else {
                    startX = 8
                    startY += button.frame.size.height
                    button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
                    startX += button.frame.size.width + 8
                }
                
                playingCell.addSubview(button)
            }
            
            return playingCell
        } else if indexPath.section == 2 {
            let commentsCell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell") as! CommentsTableViewCell
            commentsCell.commentLabel?.text = detailEvent.eventDescription
            commentsCell.selectionStyle = .none
            
            return commentsCell
        } else {
            let detailCell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell") as! DetailTableViewCell
            
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    detailCell.titleLabel?.text = "When"
                    detailCell.descriptionLabel?.text = detailEvent.starts?.shortDescription()
                } else if indexPath.row == 1 {
                    detailCell.titleLabel?.text = "Time"
                    
                    var timeString = ""
                    
                    if let startDate = detailEvent.starts {
                        timeString += startDate.timeDescription()
                    }
                    
                    if let endsDate = detailEvent.ends {
                        if timeString != "" {
                            timeString += " - " + endsDate.timeDescription()
                        } else {
                            timeString = endsDate.timeDescription()
                        }
                    }
                    
                    detailCell.descriptionLabel?.text = timeString
                } else {
                    detailCell.titleLabel?.text = "Where"
                    detailCell.descriptionLabel?.text = detailEvent.location.name
                }
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    detailCell.titleLabel?.text = "Who"
                    detailCell.descriptionLabel?.text = detailEvent.performersFullNames()
                    detailCell.titleLabelWidthConstraint?.constant = 70;
                    detailCell.layoutIfNeeded()
                } else {
                    detailCell.titleLabel?.text = "Organized by"
                    detailCell.descriptionLabel?.text = detailEvent.organizersFullNames()
                }
            }
            
            detailCell.selectionStyle = .none
            
            return detailCell
        }
    }
}

// MARK: - UITableViewDelegate

extension DetailEventViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if eventLoaded {
            return 17
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if eventLoaded {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
            headerView.backgroundColor = UIColor(red: 29.0 / 255.0, green: 30.0 / 255.0, blue: 34.0 / 255.0, alpha: 1)
            
            let redLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
            redLineView.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
            redLineView.center = headerView.center
            headerView.addSubview(redLineView)
            
            let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 0))
            headerLabel.numberOfLines = 0
            headerLabel.backgroundColor = headerView.backgroundColor
            headerLabel.textColor = redLineView.backgroundColor
            headerLabel.font = UIFont.italicSystemFont(ofSize: 16);
            if section == 0 {
                headerLabel.text = "Date and place"
            } else if section == 1 {
                headerLabel.text = "Details"
            } else {
                headerLabel.text = "Comments"
            }
            headerLabel.sizeToFit()
            headerLabel.frame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 15, height: headerLabel.frame.size.height)
            headerLabel.center = headerView.center
            headerLabel.textAlignment = .center
            
            headerView.addSubview(headerLabel)
            
            return headerView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 1 {
            var startX: CGFloat = 8
            var startY: CGFloat = 37
            
            let cellWidth = view.frame.size.width - 16
            
            for genre in detailEvent.genres {
                let button = UIButton()
                button.setTitle(genre, for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                button.sizeToFit()
                
                if cellWidth - startX - 8 >= button.frame.size.width {
                    button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
                    startX += button.frame.size.width + 8
                } else {
                    startX = 8
                    startY += button.frame.size.height
                    button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
                }
            }
            
            return startY + 34 + 8
            
        } else if indexPath.section == 2 {
            let commentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width - 16 - 16, height: 0))
            commentLabel.numberOfLines = 0
            commentLabel.font = UIFont.italicSystemFont(ofSize: 17)
            commentLabel.text = detailEvent.eventDescription
            commentLabel.textAlignment = .center
            commentLabel.lineBreakMode = .byWordWrapping
            commentLabel.sizeToFit()
            
            if commentLabel.frame.size.height == 0 {
                return 44
            } else {
                return commentLabel.frame.size.height + 22
            }
            
        } else {
            let cellWidth = view.frame.size.width - 16
            let titleLabelWidth = indexPath.section == 1 && indexPath.row == 0 ? 70 : 105
            let startX = 8 + titleLabelWidth + 8
            let descriptionLabelWidth = cellWidth - 8 - CGFloat(startX)
            
            let descriptionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: descriptionLabelWidth, height: 0))
            descriptionLabel.numberOfLines = 0
            descriptionLabel.font = UIFont.systemFont(ofSize: 17)
            descriptionLabel.textAlignment = .right
            descriptionLabel.lineBreakMode = .byWordWrapping
            
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    descriptionLabel.text = detailEvent.starts?.shortDescription()
                } else if indexPath.row == 1 {
                    var timeString = ""
                    
                    if let startDate = detailEvent.starts {
                        timeString += startDate.timeDescription()
                    }
                    
                    if let endsDate = detailEvent.ends {
                        if timeString != "" {
                            timeString += " - " + endsDate.timeDescription()
                        } else {
                            timeString = endsDate.timeDescription()
                        }
                    }
                    
                    descriptionLabel.text = timeString
                } else {
                    descriptionLabel.text = detailEvent.location.name
                }
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    descriptionLabel.text = detailEvent.performersFullNames()
                } else {
                    descriptionLabel.text = detailEvent.organizersFullNames()
                }
            }
            
            descriptionLabel.sizeToFit()
            
            if descriptionLabel.frame.size.height == 0 {
                return 44
            } else {
                return 11.5 + descriptionLabel.frame.size.height + 11.5
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 2 {
            performSegue(withIdentifier: "EventMapViewSegue", sender: nil)
        }
    }
}

// MARK: - UIScrollViewDelegate

extension DetailEventViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag != 99 {
            return
        }
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = lroundf(Float(fractionalPage))
        if currentPage != page {
            currentPage = page
            let pageControl = self.view.viewWithTag(98) as! UIPageControl
            pageControl.currentPage = currentPage
        }
    }
}
