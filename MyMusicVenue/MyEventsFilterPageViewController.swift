//
//  MyEventsFilterPageViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 25.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class MyEventsFilterPageViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            let genresTableViewCellNib = UINib(nibName: "GenresTableViewCell", bundle: nil)
            let tickTableViewCellNib = UINib(nibName: "TickTableViewCell", bundle: nil)
            
            tableView?.register(genresTableViewCellNib, forCellReuseIdentifier: "GenresTableViewCell")
            tableView?.register(tickTableViewCellNib, forCellReuseIdentifier: "TickTableViewCell")
        }
    }
    
    let sectionHeadersLabelsTextArray = ["When", "Event", "Upcoming/Past"]
    let whenLabelsTextArray = ["Today", "Now", "This week", "Month", "All"]
    let eventLabelsTextArray = ["Concert", "Festival", "Party", "Live Performance"]
    let upcomingPassedLabelsTextArray = ["Upcoming", "Past", "Past Month"]
    var tickTableViewCellsLabelsTextArray: [[String]] = []
    
    var selectedDate = ""
    var selectedEvents = Set<String>()
    var selectedUpcomingPassedEvent = ""
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tickTableViewCellsLabelsTextArray = [whenLabelsTextArray, eventLabelsTextArray, upcomingPassedLabelsTextArray]
        
        if let savedSelectedDate = UserDefaults.standard.string(forKey: "myEventsSelectedDate") {
            selectedDate = savedSelectedDate
        } else {
            selectedDate = "All"
        }
        
        if let savedSelectedEvents = UserDefaults.standard.object(forKey: "myEventsSelectedEvents") {
            selectedEvents = NSKeyedUnarchiver.unarchiveObject(with: savedSelectedEvents as! Data) as! Set<String>
        } else {
            selectedEvents = Set<String>()
        }
        
        if let savedSelectedUpcomingPassedEvents = UserDefaults.standard.string(forKey: "myEventsSelectedUpcomingPassedEvents") {
            selectedUpcomingPassedEvent = savedSelectedUpcomingPassedEvents
        } else {
            selectedUpcomingPassedEvent = "Upcoming"
        }
        
        title = "Filter"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1);
        tableView?.reloadData()
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressFilterButton() {
        UserDefaults.standard.set(selectedDate, forKey: "myEventsSelectedDate")
        UserDefaults.standard.set(selectedUpcomingPassedEvent, forKey: "myEventsSelectedUpcomingPassedEvents")
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: selectedEvents), forKey: "myEventsSelectedEvents")
        UserDefaults.standard.synchronize()
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MyEventsFilterPageGenresViewController" {
            let genresViewController = segue.destination as! GenresViewController
            genresViewController.useMyEventsGenres = true
        }
    }
}

// MARK: - UITableViewDataSource

extension MyEventsFilterPageViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 5
        } else if section == 2 {
            return 4
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let genresTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GenresTableViewCell") as! GenresTableViewCell
            genresTableViewCell.selectionStyle = .none
            
            if let genresData = UserDefaults.standard.object(forKey: "myEventsSelectedGenres") {
                let selectedGenres = NSKeyedUnarchiver.unarchiveObject(with: genresData as! Data) as? Set<String>
                genresTableViewCell.selectedGenresCountLabel?.text = "\(selectedGenres!.count)/10"
            } else {
                genresTableViewCell.selectedGenresCountLabel?.text = "0/10"
            }
            
            return genresTableViewCell
        } else {
            let tickTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TickTableViewCell") as! TickTableViewCell
            tickTableViewCell.selectionStyle = .none
            let tickTableViewCellDescriptionLabelText = tickTableViewCellsLabelsTextArray[indexPath.section - 1][indexPath.row]
            tickTableViewCell.descriptionLabel?.text = tickTableViewCellDescriptionLabelText
            
            if indexPath.section == 2 {
                if selectedEvents.contains(tickTableViewCellDescriptionLabelText) {
                    tickTableViewCell.tickImageView?.image = #imageLiteral(resourceName: "Tick")
                } else {
                    tickTableViewCell.tickImageView?.image = nil
                }
            } else {
                var isRowSelected = false
                
                if indexPath.section == 1 {
                    if selectedDate == tickTableViewCellDescriptionLabelText {
                        isRowSelected = true
                    }
                } else {
                    if selectedUpcomingPassedEvent == tickTableViewCellDescriptionLabelText {
                        isRowSelected = true
                    }
                }
                
                if isRowSelected {
                    tickTableViewCell.tickImageView?.image = #imageLiteral(resourceName: "Tick")
                } else {
                    tickTableViewCell.tickImageView?.image = nil
                }
            }
            
            return tickTableViewCell
        }
    }
}

// MARK: - UITableViewDelegate

extension MyEventsFilterPageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 17
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        headerView.backgroundColor = UIColor(red: 29.0 / 255.0, green: 30.0 / 255.0, blue: 34.0 / 255.0, alpha: 1)
        
        let redLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        redLineView.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        redLineView.center = headerView.center
        headerView.addSubview(redLineView)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 0))
        headerLabel.numberOfLines = 0
        headerLabel.backgroundColor = headerView.backgroundColor
        headerLabel.textColor = redLineView.backgroundColor
        headerLabel.font = UIFont.italicSystemFont(ofSize: 16);
        headerLabel.text = sectionHeadersLabelsTextArray[section - 1]
        headerLabel.sizeToFit()
        headerLabel.frame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 15, height: headerLabel.frame.size.height)
        headerLabel.center = headerView.center
        headerLabel.textAlignment = .center
        
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "MyEventsFilterPageGenresViewController", sender: nil)
        } else {
            let tickTableViewCellDescriptionLabelText = tickTableViewCellsLabelsTextArray[indexPath.section - 1][indexPath.row]
            
            if indexPath.section == 1 {
                selectedDate = tickTableViewCellDescriptionLabelText
            } else if indexPath.section == 2 {
                if selectedEvents.contains(tickTableViewCellDescriptionLabelText) {
                    selectedEvents.remove(tickTableViewCellDescriptionLabelText)
                } else {
                    selectedEvents.update(with: tickTableViewCellDescriptionLabelText)
                }
            } else {
                selectedUpcomingPassedEvent = tickTableViewCellDescriptionLabelText
            }
            
            UIView.transition(with: tableView, duration: 0.35, options: .transitionCrossDissolve, animations: {
                tableView.reloadData()
                }, completion: nil)
        }
    }
}
