//
//  FilterPageViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 24.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class FilterPageViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            let genresTableViewCellNib = UINib(nibName: "GenresTableViewCell", bundle: nil)
            let tickTableViewCellNib = UINib(nibName: "TickTableViewCell", bundle: nil)
            
            tableView?.register(genresTableViewCellNib, forCellReuseIdentifier: "GenresTableViewCell")
            tableView?.register(tickTableViewCellNib, forCellReuseIdentifier: "TickTableViewCell")
        }
    }
    
    let dateLabelTextArray = ["Today", "Now", "This week", "Month", "All"]
    var selectedIndex = -1
    var selectedDate = ""
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        if let savedSelectedDate = UserDefaults.standard.string(forKey: "selectedDate") {
            selectedDate = savedSelectedDate
        } else {
            selectedDate = "All"
        }
        
        if let indexOfSelectedDate = dateLabelTextArray.index(of: selectedDate) {
            selectedIndex = indexOfSelectedDate
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView?.reloadData()
    }
    
    // MARK: - Buttons Action
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressFilterPage() {
        UserDefaults.standard.set(selectedDate, forKey: "selectedDate")
        _ = navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource

extension FilterPageViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenresTableViewCell") as! GenresTableViewCell
            cell.selectionStyle = .none
            
            if let genresData = UserDefaults.standard.object(forKey: "selectedGenres") {
                let selectedGenres = NSKeyedUnarchiver.unarchiveObject(with: genresData as! Data) as? Set<String>
                cell.selectedGenresCountLabel?.text = "\(selectedGenres!.count)/10"
            } else {
                cell.selectedGenresCountLabel?.text = "0/10"
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TickTableViewCell") as! TickTableViewCell
            cell.descriptionLabel?.text = dateLabelTextArray[indexPath.row]
            cell.selectionStyle = .none
            
            if selectedIndex == indexPath.row {
                cell.tickImageView?.image = #imageLiteral(resourceName: "Tick")
            } else {
                cell.tickImageView?.image = nil
            }
            
            return cell
        }
    }
}

// MARK: - UITableViewDelegate

extension FilterPageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 17
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        headerView.backgroundColor = UIColor(red: 29.0 / 255.0, green: 30.0 / 255.0, blue: 34.0 / 255.0, alpha: 1)
        
        let redLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        redLineView.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        redLineView.center = headerView.center
        headerView.addSubview(redLineView)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 0))
        headerLabel.numberOfLines = 0
        headerLabel.backgroundColor = headerView.backgroundColor
        headerLabel.textColor = redLineView.backgroundColor
        headerLabel.font = UIFont.italicSystemFont(ofSize: 16);
        headerLabel.text = "When"
        headerLabel.sizeToFit()
        headerLabel.frame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 15, height: headerLabel.frame.size.height)
        headerLabel.center = headerView.center
        headerLabel.textAlignment = .center
        
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "FilterGenresViewController", sender: nil)
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! TickTableViewCell
            selectedDate = cell.descriptionLabel!.text!
            selectedIndex = indexPath.row
            
            UIView.transition(with: tableView, duration: 0.35, options: .transitionCrossDissolve, animations: { 
                tableView.reloadData()
                }, completion: nil)
        }
    }
}
