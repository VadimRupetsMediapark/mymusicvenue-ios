//
//  ContactUsViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 21.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController {

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressContactButton() {
        if !MFMailComposeViewController.canSendMail() {
            let alertController = UIAlertController(title: "Error", message: "Mail services are not available", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        let mailComposeViewController = MFMailComposeViewController()
        mailComposeViewController.mailComposeDelegate = self
        mailComposeViewController.setToRecipients(["weloveourusers@mymusicvenue.com"])
        
        self.present(mailComposeViewController, animated: true, completion: nil)
    }
    
}

// MARK: - Delegate

extension ContactUsViewController:MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
