//
//  AppDelegate.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 19.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        synchronizeUserDefaults()
        
        GMSServices.provideAPIKey("AIzaSyDQQgDTbRo4saxPPeF5edjxPA9RLXMUb8s")
        
        let firstLaunch = !UserDefaults.standard.bool(forKey: "firstLaunch") // false by default
        
        if firstLaunch {
            UserDefaults.standard.set(true, forKey: "firstLaunch")
            UserDefaults.standard.synchronize()
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
            let eventsViewController = storyboard.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            
            navigationController.setViewControllers([eventsViewController], animated: true)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func synchronizeUserDefaults() {
        var isSynchronizeNeeded = false
        
        if UserDefaults.standard.string(forKey: "currentCountry") == nil {
            UserDefaults.standard.set("", forKey: "currentCountry")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.object(forKey: "myEventsSelectedGenres") == nil {
            let selectedGenres = Set<String>()
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: selectedGenres), forKey: "myEventsSelectedGenres")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.string(forKey: "myEventsSelectedDate") == nil {
            UserDefaults.standard.set("All", forKey: "myEventsSelectedDate")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.object(forKey: "myEventsSelectedEvents") == nil {
            let selectedEvents = Set<String>()
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: selectedEvents), forKey: "myEventsSelectedEvents")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.string(forKey: "myEventsSelectedUpcomingPassedEvents") == nil {
            UserDefaults.standard.set("Upcoming", forKey: "myEventsSelectedUpcomingPassedEvents")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.object(forKey: "selectedGenres") == nil {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: Set<String>()), forKey: "selectedGenres")
            isSynchronizeNeeded = true
        }
        
        if UserDefaults.standard.string(forKey: "selectedDate") == nil {
            UserDefaults.standard.set("All", forKey: "selectedDate")
            isSynchronizeNeeded = true
        }
        
        if isSynchronizeNeeded {
            UserDefaults.standard.synchronize()
        }
    }
}

