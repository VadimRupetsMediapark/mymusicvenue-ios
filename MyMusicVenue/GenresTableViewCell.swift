//
//  GenresTableViewCell.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 20.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class GenresTableViewCell: UITableViewCell {
    
    @IBOutlet weak var selectedGenresCountLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
