//
//  SetPreferencesViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 19.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class SetPreferencesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView?

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My Settings"
        
        let genresTableViewCellNib = UINib(nibName: "GenresTableViewCell", bundle: nil)
        
        tableView?.register(genresTableViewCellNib, forCellReuseIdentifier: "GenresTableViewCell")
        tableView?.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1);
        tableView?.deselectRow(at: IndexPath(row: 0, section: 0), animated: true)
        tableView?.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tableView?.deselectRow(at: IndexPath(row: 0, section: 0), animated: true)
        
        navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }

}

// MARK: - UITableViewDataSource

extension SetPreferencesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenresTableViewCell") as! GenresTableViewCell
            cell.selectionStyle = .none
            
            if let genresData = UserDefaults.standard.object(forKey: "selectedGenres") {
                let selectedGenres = NSKeyedUnarchiver.unarchiveObject(with: genresData as! Data) as? Set<String>
                cell.selectedGenresCountLabel?.text = "\(selectedGenres!.count)/10"
            } else {
                cell.selectedGenresCountLabel?.text = "0/10"
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenresTableViewCell") as! GenresTableViewCell
            cell.selectionStyle = .none
            cell.selectedGenresCountLabel?.text = ""
            
            if let addressString = UserDefaults.standard.string(forKey: "currentAddress") {
                cell.descriptionLabel?.text = addressString
            } else {
                cell.descriptionLabel?.text = "Set My Location"
            }
            
            return cell
        }
    }
}

// MARK: - UITableViewDelegate

extension SetPreferencesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 17;
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        headerView.backgroundColor = UIColor.clear
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 0))
        headerLabel.numberOfLines = 0
        headerLabel.backgroundColor = UIColor.clear
        headerLabel.textColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        headerLabel.font = UIFont.italicSystemFont(ofSize: 16);
        headerLabel.text = "Set My Location"
        headerLabel.sizeToFit()
        headerLabel.frame = CGRect(x: 0, y: 0, width: headerLabel.frame.size.width + 15, height: headerLabel.frame.size.height)
        headerLabel.center = headerView.center
        headerLabel.textAlignment = .center
        
        let leftLineView = UIView(frame: CGRect(x: 0, y: 10, width: headerLabel.frame.origin.x - 10, height: 1))
        leftLineView.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        headerView.addSubview(leftLineView)
        
        let rightLineView = UIView(frame: CGRect(x: headerLabel.frame.origin.x + headerLabel.frame.size.width + 8, y: 10, width: headerView.frame.size.width - headerLabel.frame.size.width + 8, height: 1))
        rightLineView.backgroundColor = leftLineView.backgroundColor
        headerView.addSubview(rightLineView)
        
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "GenresViewController", sender: nil)
        } else {
            performSegue(withIdentifier: "PreferencesMapsViewController", sender: nil)
        }
    }
}
