//
//  DetailEventModel.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 28.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import Foundation

class DetailEvent: NSObject, NSCoding {
    let ends: Date?
    let name: String
    let performers: [Person]
    let genres: [String]
    let eventDescription: String
    let rating: Double
    let type: String
    let updated: Date?
    let location: Location
    let id: EventId
    let created: Date?
    let mediaFiles: [MediaFile]
    let organizers: [Person]
    let starts: Date?
    
    override init() {
        self.ends = nil
        self.name = ""
        self.performers = [Person]()
        self.genres = [""]
        self.eventDescription = ""
        self.rating = 0
        self.type = ""
        self.updated = nil
        self.location = Location()
        self.id = EventId()
        self.created = nil
        self.mediaFiles = [MediaFile]()
        self.organizers = [Person]()
        self.starts = nil
        
        super.init()
    }
    
    init(dictionary: [String: Any]) {
        
        // Ends date parsing
        
        if let endsString = dictionary["ends"] as? NSString {
            var timezoneString = endsString.substring(with: NSMakeRange(endsString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = endsString.substring(with: NSMakeRange(0, endsString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + " " + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.ends = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.ends = nil
        }
        
        // Name parsing
        
        if let nameString = dictionary["name"] as? String {
            self.name = nameString
        } else {
            self.name = ""
        }
        
        // Performers array parsing
        
        if let performersArray = dictionary["performers"] as? [[String: String]] {
            var performers = [Person]()
            
            for performer in performersArray {
                var email = ""
                var facebook = ""
                var firstName = ""
                var instagram = ""
                var lastName = ""
                var middleInitial = ""
                var snapchat = ""
                var twitter = ""
                var username = ""
                
                if let emailString = performer["email"] {
                    email = emailString
                }
                
                if let facebookString = performer["facebook"] {
                    facebook = facebookString
                }
                
                if let firstNameString = performer["first_name"] {
                    firstName = firstNameString
                }
                
                if let instagramString = performer["instagram"] {
                    instagram = instagramString
                }
                
                if let lastNameString = performer["last_name"] {
                    lastName = lastNameString
                }
                
                if let middleInitialString = performer["middle_initial"] {
                    middleInitial = middleInitialString
                }
                
                if let snapchatString = performer["snapchat"] {
                    snapchat = snapchatString
                }
                
                if let twitterString = performer["twitter"] {
                    twitter = twitterString
                }
                
                if let usernameString = performer["username"] {
                    username = usernameString
                }
                
                performers.append(Person.init(email: email, facebook: facebook, firstName: firstName, instagram: instagram, lastName: lastName, middleInitial: middleInitial, snapchat: snapchat, twitter: twitter, username: username))
            }
            
            self.performers = performers
        } else {
            self.performers = [Person]()
        }
        
        // Genres parsing
        
        if let genresArray = dictionary["genres"] as? [String] {
            self.genres = genresArray
        } else {
            self.genres = [String]()
        }
        
        // Description parsing
        
        if let descriptionString = dictionary["description"] as? String {
            self.eventDescription = descriptionString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        } else {
            self.eventDescription = ""
        }
        
        // Rating parsing
        
        if let ratingDouble = dictionary["ration"] as? Double {
            self.rating = ratingDouble
        } else {
            self.rating = 0
        }
        
        // Type parsing
        
        if let typeString = dictionary["type"] as? String {
            self.type = typeString
        } else {
            self.type = ""
        }
        
        // Updated Date parsing
        
        if let updatedDateString = dictionary["updated"] as? NSString {
            var timezoneString = updatedDateString.substring(with: NSMakeRange(updatedDateString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = updatedDateString.substring(with: NSMakeRange(0, updatedDateString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + "" + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.updated = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.updated = nil
        }
        
        // Location parsing
        
        if let locationDictionary = dictionary["location"] as? [String: Any] {
            var address1 = ""
            var address2 = ""
            var city = ""
            var description = ""
            var locationLatitude: Double = 0
            var locationLongitude: Double = 0
            var name = ""
            var state = ""
            var zip = ""
            
            if let address1String = locationDictionary["address_1"] as? String {
                address1 = address1String
            }
            
            if let address2String = locationDictionary["address_2"] as? String {
                address2 = address2String
            }
            
            if let cityString = locationDictionary["city"] as? String {
                city = cityString
            }
            
            if let descriptionString = locationDictionary["description"] as? String {
                description = descriptionString
            }
            
            if let coordinateDictionary = locationDictionary["location"] as? [String: Double] {
                
                if let latitude = coordinateDictionary["lat"] {
                    locationLatitude = latitude
                }
                
                if let longitude = coordinateDictionary["lng"] {
                    locationLongitude = longitude
                }
            }
            
            if let nameString = locationDictionary["name"] as? String {
                name = nameString
            }
            
            if let stateString = locationDictionary["state"] as? String {
                state = stateString
            }
            
            if let zipString = locationDictionary["zip"] as? String {
                zip = zipString
            }
            
            self.location = Location(address1: address1, address2: address2, city: city, description: description, latitude: locationLatitude, longitude: locationLongitude, name: name, state: state, zip: zip)
        } else {
            self.location = Location()
        }
        
        // Event id parsing
        
        if let eventIdDictionary = dictionary["_id"] as? [String: String] {
            if let eventId = eventIdDictionary["$id"] {
                self.id = EventId(eventId: eventId)
            } else {
                self.id = EventId()
            }
        } else {
            self.id = EventId()
        }
        
        // Created date parsing
        
        if let createdString = dictionary["created"] as? NSString {
            var timezoneString = createdString.substring(with: NSMakeRange(createdString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = createdString.substring(with: NSMakeRange(0, createdString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + " " + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.created = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.created = nil
        }
        
        // MediaFiles parsing
        
        if let mediaFilesArray = dictionary["media_files"] as? [[String: String]] {
            var mediaFiles = [MediaFile]()
            
            for mediaFile in mediaFilesArray {
                var mediaFileURL = ""
                var thumbnailURL = ""
                var type = ""
                
                if let dictionaryMediaFileURL = mediaFile["media_file_url"] {
                    mediaFileURL = dictionaryMediaFileURL
                }
                
                if let dictionaryThumbnailURL = mediaFile["thumbnail_url"] {
                    thumbnailURL = dictionaryThumbnailURL
                }
                
                if let dictionaryType = mediaFile["type"] {
                    type = dictionaryType
                }
                
                mediaFiles.append(MediaFile(mediaFileURL: mediaFileURL, thumbnailURL: thumbnailURL, type: type))
            }
            
            self.mediaFiles = mediaFiles
        } else {
            self.mediaFiles = [MediaFile]()
        }
        
        // Organizers parsing 
        
        if let organizersArray = dictionary["organizers"] as? [[String: String]] {
            var organizers = [Person]()
            
            for organizer in organizersArray {
                var email = ""
                var facebook = ""
                var firstName = ""
                var instagram = ""
                var lastName = ""
                var middleInitial = ""
                var snapchat = ""
                var twitter = ""
                var username = ""
                
                if let emailString = organizer["email"] {
                    email = emailString
                }
                
                if let facebookString = organizer["facebook"] {
                    facebook = facebookString
                }
                
                if let firstNameString = organizer["first_name"] {
                    firstName = firstNameString
                }
                
                if let instagramString = organizer["instagram"] {
                    instagram = instagramString
                }
                
                if let lastNameString = organizer["last_name"] {
                    lastName = lastNameString
                }
                
                if let middleInitialString = organizer["middle_initial"] {
                    middleInitial = middleInitialString
                }
                
                if let snapchatString = organizer["snapchat"] {
                    snapchat = snapchatString
                }
                
                if let twitterString = organizer["twitter"] {
                    twitter = twitterString
                }
                
                if let usernameString = organizer["username"] {
                    username = usernameString
                }
                
                organizers.append(Person.init(email: email, facebook: facebook, firstName: firstName, instagram: instagram, lastName: lastName, middleInitial: middleInitial, snapchat: snapchat, twitter: twitter, username: username))
            }
            
            self.organizers = organizers
        } else {
            self.organizers = [Person]()
        }
        
        // Starts date parsing
        
        if let startsString = dictionary["starts"] as? NSString {
            var timezoneString = startsString.substring(with: NSMakeRange(startsString.length - 6, 6))
            timezoneString = timezoneString.replacingOccurrences(of: ":", with: "")
            
            var restString = startsString.substring(with: NSMakeRange(0, startsString.length - 6))
            restString = restString.replacingOccurrences(of: "T", with: " ")
            
            let fullDateString = restString + " " + timezoneString
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            
            self.starts = dateFormatter.date(from: fullDateString)?.addingTimeInterval(60 * 60 * 3)
        } else {
            self.starts = nil
        }
        
        super.init()
    }
    
    init(ends: Date?, name: String, performers: [Person], genres: [String], eventDescription: String, rating: Double, type: String, updated: Date?, location: Location, id: EventId, created: Date?, mediaFiles: [MediaFile], organizers: [Person], starts: Date?) {
        self.ends = ends
        self.name = name
        self.performers = performers
        self.genres = genres
        self.eventDescription = eventDescription
        self.rating = rating
        self.type = type
        self.updated = updated
        self.location = location
        self.id = id
        self.created = created
        self.mediaFiles = mediaFiles
        self.organizers = organizers
        self.starts = starts
        
        super.init()
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let ends = aDecoder.decodeObject(forKey: "ends") as? Date
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let performers = aDecoder.decodeObject(forKey: "performers") as! [Person]
        let genres = aDecoder.decodeObject(forKey: "genres") as! [String]
        let eventDescription = aDecoder.decodeObject(forKey: "eventDescription") as! String
        let rating = aDecoder.decodeDouble(forKey: "rating")
        let type = aDecoder.decodeObject(forKey: "type") as! String
        let updated = aDecoder.decodeObject(forKey: "updated") as? Date
        let location = aDecoder.decodeObject(forKey: "location") as! Location
        let id = aDecoder.decodeObject(forKey: "id") as! EventId
        let created = aDecoder.decodeObject(forKey: "created") as? Date
        let mediaFiles = aDecoder.decodeObject(forKey: "mediaFiles") as! [MediaFile]
        let organizers = aDecoder.decodeObject(forKey: "organizers") as! [Person]
        let starts = aDecoder.decodeObject(forKey: "starts") as? Date
        
        self.init(ends: ends, name: name, performers: performers, genres: genres, eventDescription: eventDescription, rating: rating, type: type, updated: updated, location: location, id: id, created: created, mediaFiles: mediaFiles, organizers: organizers, starts: starts)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(ends, forKey: "ends")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(performers, forKey: "performers")
        aCoder.encode(genres, forKey: "genres")
        aCoder.encode(eventDescription, forKey: "eventDescription")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(updated, forKey: "updated")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(created, forKey: "created")
        aCoder.encode(mediaFiles, forKey: "mediaFiles")
        aCoder.encode(organizers, forKey: "organizers")
        aCoder.encode(starts, forKey: "starts")
    }
}

class Person: NSObject, NSCoding {
    let email: String
    let facebook: String
    let firstName: String
    let instagram: String
    let lastName: String
    let middleInitial: String
    let snapchat: String
    let twitter: String
    let username: String
    
    override init() {
        self.email = ""
        self.facebook = ""
        self.firstName = ""
        self.instagram = ""
        self.lastName = ""
        self.middleInitial = ""
        self.snapchat = ""
        self.twitter = ""
        self.username = ""
        
        super.init()
    }
    
    init(email: String, facebook: String, firstName: String, instagram: String, lastName: String, middleInitial: String, snapchat: String, twitter: String, username: String) {
        self.email = email
        self.facebook = facebook
        self.firstName = firstName
        self.instagram = instagram
        self.lastName = lastName
        self.middleInitial = middleInitial
        self.snapchat = snapchat
        self.twitter = twitter
        self.username = username
        
        super.init()
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let facebook = aDecoder.decodeObject(forKey: "facebook") as! String
        let firstName = aDecoder.decodeObject(forKey: "firstName") as! String
        let instagram = aDecoder.decodeObject(forKey: "instagram") as! String
        let lastName = aDecoder.decodeObject(forKey: "lastName") as! String
        let middleInitial = aDecoder.decodeObject(forKey: "middleInitial") as! String
        let snapchat = aDecoder.decodeObject(forKey: "snapchat") as! String
        let twitter = aDecoder.decodeObject(forKey: "twitter") as! String
        let username = aDecoder.decodeObject(forKey: "username") as! String
        
        self.init(email: email, facebook: facebook, firstName: firstName, instagram: instagram, lastName: lastName, middleInitial: middleInitial, snapchat: snapchat, twitter: twitter, username: username)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: "email")
        aCoder.encode(facebook, forKey: "facebook")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(instagram, forKey: "instagram")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(middleInitial, forKey: "middleInitial")
        aCoder.encode(snapchat, forKey: "snapchat")
        aCoder.encode(twitter, forKey: "twitter")
        aCoder.encode(username, forKey: "username")
    }
}

extension DetailEvent {
    func performersFullNames() -> String {
        return personsFullNames(persons: self.performers)
    }
    
    func organizersFullNames() -> String {
        return personsFullNames(persons: self.organizers)
    }
    
    private func personsFullNames(persons: [Person]) -> String {
        var personsFullNames = ""
        
        for person in persons {
            let personFullName = person.fullName()
            
            if personFullName == "" {
                continue
            } else {
                if personsFullNames != "" {
                    personsFullNames += " "
                }
                
                personsFullNames += personFullName
            }
        }
        
        return personsFullNames
    }
}

extension Person {
    func fullName() -> String {
        var fullName = ""
        
        if self.firstName != "" {
            fullName += self.firstName
        }
        
        if self.lastName != "" {
            if fullName != "" {
                fullName += " "
            }
            
            fullName += self.lastName
        }
        
        return fullName
    }
}
