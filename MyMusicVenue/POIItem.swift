//
//  POIItem.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 31.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import Foundation

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var event: Event
    var detailEvent: DetailEvent
    
    init(position: CLLocationCoordinate2D, event: Event) {
        self.position = position
        self.event = event
        self.detailEvent = DetailEvent()
        
        super.init()
    }
    
    init(position: CLLocationCoordinate2D, detailEvent: DetailEvent) {
        self.position = position
        self.event = Event()
        self.detailEvent = detailEvent
    }
}
