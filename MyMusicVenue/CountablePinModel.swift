//
//  CountablePinModel.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 31.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import Foundation
import CoreLocation

class CountablePin: NSObject {
    let coordinate: CLLocationCoordinate2D
    var eventCount: Int
    
    override init() {
        self.coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        self.eventCount = 0
        
        super.init()
    }
    
    init(coordinate: CLLocationCoordinate2D, eventCount: Int) {
        self.coordinate = coordinate
        self.eventCount = eventCount
        
        super.init()
    }
}
