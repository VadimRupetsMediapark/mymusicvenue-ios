//
//  TutorialPageViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 19.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIPageViewController {
    
    fileprivate(set) lazy var tutorialViewControllers: [UIViewController] = {
            return [self.tutorialViewController("First"), self.tutorialViewController("Second"), self.tutorialViewController("Third")]
        }()
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: 207.0 / 255.0, green: 52.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        
        dataSource = self
        
        navigationController?.isNavigationBarHidden = true
        
        if let firstViewController = tutorialViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper
    
    fileprivate func tutorialViewController(_ numeral: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Tutorial\(numeral)ViewController")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UIPageViewControllerDataSource

extension TutorialPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = tutorialViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousViewControllerIndex = viewControllerIndex - 1
        
        guard previousViewControllerIndex >= 0 else {
            return nil
        }
        
        guard tutorialViewControllers.count > previousViewControllerIndex else {
            return nil
        }
        
        return tutorialViewControllers[previousViewControllerIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = tutorialViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextViewControllerIndex = viewControllerIndex + 1
        let tutorialViewControllersCount = tutorialViewControllers.count
        
        guard tutorialViewControllersCount != nextViewControllerIndex else {
            return nil
        }
        
        guard tutorialViewControllersCount > nextViewControllerIndex else {
            return nil
        }
        
        return tutorialViewControllers[nextViewControllerIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return tutorialViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = tutorialViewControllers.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
}
