//
//  PlayingTableViewCell.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 25.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class PlayingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addGenresButtons(_ genresArray: [String]) {
        var startX: CGFloat = 8
        var startY: CGFloat = 37
        
        let cellWidth = self.frame.size.width
        
        for genre in genresArray {
            let button = UIButton()
            button.setTitle(genre, for: .normal)
            button.setTitleColor(UIColor.init(red: 213.0 / 255.0, green: 12.0 / 255.0, blue: 23.0 / 255.0, alpha: 1), for: .normal)
            button.sizeToFit()
            
            if cellWidth - startX - 8 >= button.frame.size.width {
                button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
                startX += button.frame.size.width + 8
            } else {
                startX = 8
                startY += button.frame.size.height + 8
                button.frame = CGRect(x: startX, y: startY, width: button.frame.size.width, height: button.frame.size.height)
            }
            
            self.addSubview(button)
        }
    }
    
}
