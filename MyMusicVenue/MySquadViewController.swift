//
//  MySquadViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 24.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class MySquadViewController: UIViewController {

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
