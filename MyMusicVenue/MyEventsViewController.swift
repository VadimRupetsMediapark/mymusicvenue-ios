//
//  MyEventsViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 25.10.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit
import JGProgressHUD

class MyEventsViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var mainViewLeftConstraint: NSLayoutConstraint?
    @IBOutlet weak var mainViewRightConstraint: NSLayoutConstraint?
    @IBOutlet weak var searchTextField: UITextField? {
        didSet {
            searchTextField?.textColor = UIColor.white
            searchTextField?.attributedPlaceholder = NSAttributedString.init(string: "Search", attributes: [NSForegroundColorAttributeName: UIColor.white])
            searchTextField?.tintColor = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 19.0 / 255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var filterOrDeleteButton: UIButton?
    var isFilterButtonFilter = true
    
    @IBOutlet weak var mapOrListButton: UIButton? {
        didSet {
            mapOrListButton?.imageView?.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var mapOrListButtonHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var mapOrListButtonBottomConstraint: NSLayoutConstraint?
    var isMapButtonMap = false
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            let tableViewNib = UINib(nibName: "EventTableViewCell", bundle: nil)
            tableView?.register(tableViewNib, forCellReuseIdentifier: "EventTableViewCell")
        }
    }
    
    @IBOutlet weak var mapView: GMSMapView? {
        didSet {
            mapView?.delegate = self
        }
    }
    
    @IBOutlet weak var mapViewEventsPopup: UIView? {
        didSet {
            mapViewEventsPopup?.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            mapViewEventsPopup?.layer.cornerRadius = 10
            mapViewEventsPopup?.layer.borderWidth = 2
            mapViewEventsPopup?.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var mapViewEventsPopupBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var mapViewEventsPopupTopConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var mapViewEventsPopupTableView: UITableView? {
        didSet {
            let tableViewNib = UINib(nibName: "EventTableViewCell", bundle: nil)
            mapViewEventsPopupTableView?.register(tableViewNib, forCellReuseIdentifier: "EventTableViewCell")
        }
    }
    
    var clusterManager: GMUClusterManager!
    var clusterEventsArray = [DetailEvent]()
    
    var myEvents = [DetailEvent]()
    
    var searchResultArray = [DetailEvent]()
    var useSearchResults = false
    
    var myEventsSelectedGenres = Set<String>()
    var myEventsSelectedDate = ""
    var myEventsSelectedEvents = Set<String>()
    var myEventsSelectedUpcomingPassedEvents = ""

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView!, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView!, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
        searchTextField?.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
        
        refreshFilters()
        
        refreshMyEvents()
        filterMyEvents()
        
        refreshCluster()
        
        tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - MyEvents methods
    
    func refreshMyEvents() {
        if let savedMyEventsData = UserDefaults.standard.object(forKey: "myEvents") as? Data {
            if let savedMyEvents = NSKeyedUnarchiver.unarchiveObject(with: savedMyEventsData) as? [DetailEvent] {
                myEvents = savedMyEvents
            } else {
                myEvents = [DetailEvent]()
            }
        } else {
            myEvents = [DetailEvent]()
        }
    }
    
    func refreshFilters() {
        if let savedSelectedGenresData = UserDefaults.standard.object(forKey: "myEventsSelectedGenres") as? Data {
            if let savedSelectedGenresFilter = NSKeyedUnarchiver.unarchiveObject(with: savedSelectedGenresData) as? Set<String> {
                myEventsSelectedGenres = savedSelectedGenresFilter
            }
        }
        
        if let savedSelectedDateFilter = UserDefaults.standard.string(forKey: "myEventsSelectedDate") {
            myEventsSelectedDate = savedSelectedDateFilter
        }
        
        if let savedSelectedEventsData = UserDefaults.standard.object(forKey: "myEventsSelectedEvents") as? Data {
            if let savedSelectedEventsFilter = NSKeyedUnarchiver.unarchiveObject(with: savedSelectedEventsData) as? Set<String> {
                myEventsSelectedEvents = savedSelectedEventsFilter
            }
        }
        
        if let savedSelectedUpcomingPassedEventsFilter = UserDefaults.standard.string(forKey: "myEventsSelectedUpcomingPassedEvents") {
            myEventsSelectedUpcomingPassedEvents = savedSelectedUpcomingPassedEventsFilter
        }
    }
    
    func myEventsContains(specificEvent: DetailEvent) -> Bool {
        for event in myEvents {
            if event.id.id == specificEvent.id.id {
                return true
            }
        }
        
        return false
    }
    
    func myEventsRemove(specificEvent: DetailEvent) {
        var indexForRemoving = -1
        
        for (index, event) in myEvents.enumerated() {
            if event.id.id == specificEvent.id.id {
                indexForRemoving = index
                break
            }
        }
        
        myEvents.remove(at: indexForRemoving)
    }
    
    // MARK: - Filter events
    
    func filterMyEvents() {
        let currentDate = Date()
        
        if myEventsSelectedUpcomingPassedEvents == "Upcoming" || myEventsSelectedUpcomingPassedEvents == "Past" {
            let isNeededUpcoming = myEventsSelectedUpcomingPassedEvents == "Upcoming" ? true : false
            
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                return (detailEvent.starts! > currentDate) == isNeededUpcoming
            })
        } else {
            let monthTimeInterval: Double = 60 * 60 * 24 * 30
            
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                return (detailEvent.starts! < currentDate) && (currentDate.timeIntervalSince(detailEvent.starts!) < monthTimeInterval)
            })
        }
        
        if myEventsSelectedDate != "All" && myEventsSelectedDate != "" {
            filterBySelectedDate()
        }
        
        if myEventsSelectedGenres.count != 0 && myEventsSelectedGenres.count != 10 {
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                let genresSet = Set(detailEvent.genres)
                return myEventsSelectedGenres.intersection(genresSet).count > 0
            })
        }
        
        if myEventsSelectedEvents.count != 0 && myEventsSelectedEvents.count != 4 {
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                return myEventsSelectedEvents.contains(detailEvent.type)
            })
        }
    }
    
    func filterBySelectedDate() {
        let currentDate = Date()
        
        let currentDayNumber = Calendar.current.component(.day, from: currentDate)
        let currentDayMonth = Calendar.current.component(.month, from: currentDate)
        let currentDayYear = Calendar.current.component(.year, from: currentDate)
        
        switch myEventsSelectedDate {
        case "Today":
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                let eventDayNumber = Calendar.current.component(.day, from: detailEvent.starts!)
                let eventMonth = Calendar.current.component(.month, from: detailEvent.starts!)
                let eventYear = Calendar.current.component(.year, from: detailEvent.starts!)
                
                return currentDayNumber == eventDayNumber && currentDayMonth == eventMonth && currentDayYear == eventYear
            })
        case "Now":
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                let eventDayNumber = Calendar.current.component(.day, from: detailEvent.starts!)
                let eventMonth = Calendar.current.component(.month, from: detailEvent.starts!)
                let eventYear = Calendar.current.component(.year, from: detailEvent.starts!)
                
                return currentDayNumber == eventDayNumber && currentDayMonth == eventMonth && currentDayYear == eventYear && currentDate > detailEvent.starts!
            })
        case "This week":
            let currentWeekMondayDate = Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
            
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                return Calendar.current.component(.weekOfYear, from: detailEvent.starts!) == Calendar.current.component(.weekOfYear, from: currentWeekMondayDate)
            })
        default:
            let currentMonthFirstDate = Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.year, .month], from: Date()))!
            
            myEvents = myEvents.filter({ (detailEvent) -> Bool in
                return Calendar.current.component(.month, from: detailEvent.starts!) == Calendar.current.component(.month, from: currentMonthFirstDate) && Calendar.current.component(.year, from: detailEvent.starts!) == Calendar.current.component(.year, from: currentMonthFirstDate)
            })
        }
    }
    
    // MARK: - Refresh MapView Cluster
    
    func refreshCluster() {
        clusterManager.clearItems()
        
        if let firstEvent = useSearchResults ? searchResultArray.first : myEvents.first {
            let cameraUpdate = GMSCameraUpdate.setTarget(firstEvent.location.location, zoom: 10)
            mapView?.animate(with: cameraUpdate)
        }
        
        for event in useSearchResults ? searchResultArray : myEvents {
            let item = POIItem(position: event.location.location, detailEvent: event)
            clusterManager.add(item)
        }
        
        clusterManager.cluster()
    }
    
    // MARK: - Menu show/hide func
    
    func showMenu() {
        mainViewLeftConstraint?.constant = 150
        mainViewRightConstraint?.constant -= 150
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideMenu() {
        mainViewLeftConstraint?.constant = 0
        mainViewRightConstraint?.constant = 0
        
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - TextField Event
    
    func textFieldDidChange(textField: UITextField) {
        if textField.text == "" {
            filterOrDeleteButton?.setImage(#imageLiteral(resourceName: "Filter"), for: .normal)
            isFilterButtonFilter = true
            
            useSearchResults = false
            textField.resignFirstResponder()
        } else {
            filterOrDeleteButton?.setImage(#imageLiteral(resourceName: "X icon"), for: .normal)
            isFilterButtonFilter = false
            
            searchResultArray = myEvents.filter({ (detailEvent) -> Bool in
                return detailEvent.name.localizedCaseInsensitiveContains(textField.text!)
            })
            useSearchResults = true
            
        }
        
        tableView?.reloadData()
        refreshCluster()
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressMenuButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if mainViewLeftConstraint?.constant == 0 {
            showMenu()
        } else {
            hideMenu()
        }
        
        searchTextField?.resignFirstResponder()
    }
    
    @IBAction func didPressFilterOrDeleteButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if isFilterButtonFilter {
            performSegue(withIdentifier: "MyEventsFilterPageViewController", sender: nil)
        } else {
            searchTextField?.text = ""
            useSearchResults = false
            
            filterOrDeleteButton?.setImage(#imageLiteral(resourceName: "Filter"), for: .normal)
            isFilterButtonFilter = true
            
            tableView?.reloadData()
            refreshCluster()
        }
    }
    
    @IBAction func didPressMapOrListButton() {
        if mapViewEventsPopup?.alpha == 1.0 {
            return
        }
        
        if isMapButtonMap {
            mapOrListButtonHeightConstraint?.constant = 39
            mapOrListButtonBottomConstraint?.constant -= 6
            mapOrListButton?.setImage(#imageLiteral(resourceName: "Map Icon"), for: .normal)
            self.view.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.35, animations: {
                self.mapView?.alpha = 0.0
            }, completion: { (completion) in
                self.mapView?.isHidden = true
            })
        } else {
            mapOrListButtonHeightConstraint?.constant = 26
            mapOrListButtonBottomConstraint?.constant += 6
            mapOrListButton?.setImage(#imageLiteral(resourceName: "List"), for: .normal)
            self.view.layoutIfNeeded()
            
            refreshCluster()
            
            mapView?.isHidden = false
            UIView.animate(withDuration: 0.35, animations: {
                self.mapView?.alpha = 1.0
            })
        }
        
        isMapButtonMap = !isMapButtonMap
    }
    
    @IBAction func didPressMapViewEventsClosePopupButton() {
        UIView.animate(withDuration: 0.5, animations: {
            self.mapViewEventsPopup?.alpha = 0.0
        })
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        mainViewLeftConstraint?.constant = 0
        mainViewRightConstraint?.constant = 0
        
        UIView.animate(withDuration: 1.5) {
            self.view.layoutIfNeeded()
            self.mapViewEventsPopup?.alpha = 0.0
        }
        
        if segue.identifier == "MyEventsDetailEventSegue" {
            let detailEventViewController = segue.destination as! DetailEventViewController
            detailEventViewController.detailEvent = sender as! DetailEvent
        }
    }

}

// MARK: - UITextFieldDelegate

extension MyEventsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if mapViewEventsPopup?.alpha == 1.0 {
            return false
        } else {
            return true
        }
    }
}

// MARK: - UITableViewDataSource

extension MyEventsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 5 {
            return clusterEventsArray.count
        } else if useSearchResults {
            return searchResultArray.count
        } else {
            return myEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as! EventTableViewCell
        
        var detailEvent: DetailEvent
        if tableView.tag == 5 {
            detailEvent = clusterEventsArray[indexPath.row]
        } else if useSearchResults {
            detailEvent = searchResultArray[indexPath.row]
        } else {
            detailEvent = myEvents[indexPath.row]
        }
        
        var genresString = ""
        
        for genre in detailEvent.genres {
            genresString += genre + " "
        }
        
        var dateLabelText = ""
        if let eventDate = detailEvent.starts {
            dateLabelText += eventDate.shortDescription()
        }
        
        cell.artistLabel?.text = detailEvent.name
        cell.placeLabel?.text = detailEvent.location.name
        cell.genreLabel?.text = genresString
        cell.dateLabel?.text = dateLabelText
        
//        if let thumbnailURL = detailEvent.mediaFiles.first?.thumbnailURL {
//            cell.artistLogoImageView?.sd_setImage(with: URL(string: thumbnailURL))
//        }
        cell.artistLogoImageView?.image = #imageLiteral(resourceName: "Event thumbnail")
        
        cell.selectionStyle = .none
        
        return cell
    }
}

extension MyEventsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MyEventsDetailEventSegue", sender: tableView.tag == 5 ? clusterEventsArray[indexPath.row] : myEvents[indexPath.row])
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchTextField?.resignFirstResponder()
    }
}

// MARK: - MapViewMethods

extension MyEventsViewController {
    func didTap(clusterItems: [POIItem]) {
        let progressHUD = JGProgressHUD.init(style: .dark)
        progressHUD?.textLabel.text = "Loading"
        progressHUD?.show(in: view)
        
        DispatchQueue.global(qos: .background).async {
            self.clusterEventsArray = [DetailEvent]()
            
            for poiItem in clusterItems {
                self.clusterEventsArray.append(poiItem.detailEvent)
            }
            
            var missedEvents = [DetailEvent]()
            
            for event in self.clusterEventsArray {
                let samePositionEvents = (self.useSearchResults ? self.searchResultArray : self.myEvents).filter({ (specificEvent) -> Bool in
                    return specificEvent.location.location.latitude == event.location.location.latitude && specificEvent.location.location.longitude == event.location.location.longitude
                })
                
                missedEvents.append(contentsOf: samePositionEvents)
            }
            
            self.clusterEventsArray.append(contentsOf: missedEvents)
            self.clusterEventsArray = Array<DetailEvent>(Set<DetailEvent>(self.clusterEventsArray))
            
            let cellsHeight = self.clusterEventsArray.count * 100 + 16
            let constraintConstant = (self.view.frame.size.height - 25 - 8 - 16 - 40 - CGFloat(cellsHeight)) / 2.0
            
            DispatchQueue.main.async {
                self.mapViewEventsPopupTableView?.reloadData()
                
                if constraintConstant > 0 {
                    self.mapViewEventsPopupTableView?.sizeToFit()
                    self.mapViewEventsPopupTableView?.isScrollEnabled = false
                    
                    let constant = (self.view.frame.size.height - CGFloat(cellsHeight)) / 2.0
                    
                    self.mapViewEventsPopupTopConstraint?.constant = constant - 28
                    self.mapViewEventsPopupBottomConstraint?.constant = constant - 28
                } else {
                    self.mapViewEventsPopupTopConstraint?.constant = 25
                    self.mapViewEventsPopupBottomConstraint?.constant = 8
                    
                    self.mapViewEventsPopupTableView?.isScrollEnabled = true
                }
                
                self.mapViewEventsPopup?.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.35, animations: {
                    self.mapViewEventsPopup?.alpha = 1.0
                }, completion: { (completion) in
                    progressHUD?.dismiss(animated: true)
                })
            }
        }
    }
}

extension MyEventsViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            didTap(clusterItems: [poiItem])
        } else {
            print("marker")
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if gesture {
            searchTextField?.resignFirstResponder()
        }
    }
}

extension MyEventsViewController: GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) {
        if let clusterItems = cluster.items as? [POIItem] {
            didTap(clusterItems: clusterItems)
        }
    }
}
