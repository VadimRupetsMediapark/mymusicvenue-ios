//
//  EventMapViewController.swift
//  MyMusicVenue
//
//  Created by Vadim Rupets on 21.11.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

class EventMapViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var mapView: GMSMapView?
    
    var eventTitle = ""
    var eventLocation = CLLocationCoordinate2D(latitude: 0, longitude: 0)

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel?.text = eventTitle
        setupMapView()
    }
    
    func setupMapView() {
        let camera = GMSCameraPosition.camera(withTarget: eventLocation, zoom: 14)
        mapView?.animate(to: camera)
        
        let marker = GMSMarker(position: eventLocation)
        marker.map = mapView
        marker.snippet = eventTitle
    }
    
    // MARK: - Buttons Actions
    
    @IBAction func didPressBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }

}
